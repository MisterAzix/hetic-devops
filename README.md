# HETIC DevOps

## Infos

- [x] Workflow Git Flow
- [x] Register Gitlab-Runner

## Team

- Maxence BREUILLES ([@MisterAzix](https://github.com/MisterAzix))
- Justin KATASI ([@justinDev91](https://github.com/justinDev91/))
- Julian LABALLE ([@Triips-TheCoder](https://github.com/triips-thecoder/))
- Benoît FAVRIE ([@Mowdyy](https://github.com/Mowdyy))
- Louis FORTRIE ([@louisFortrie](https://github.com/louisFortrie))
- Anis COQUELET ([@aniscoquelet](https://github.com/aniscoquelet))
